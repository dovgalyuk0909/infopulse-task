class Main {
	constructor() {
		this.model = [
			{projectName: "first project", dueDate: "13.09.2017",created:"12.04.2017",members:"oleg",type:"web",status:"done",customer:"john",icon:""},
			{projectName: "second project", dueDate: "13.03.2017",created:"12.02.2017",members:"john",type:"mobile",status:"in progres",customer:"bill",icon:""},
			{projectName: "third project", dueDate: "13.01.2017",created:"10.01.2017",members:"mark",type:"desctop",status:"done",customer:"david",icon:""},
			{projectName: "fourth project", dueDate: "13.04.2017",created:"12.02.2017",members:"ann",type:"support",status:"in progres",customer:"alexandro Rohas",icon:""},
			{projectName: "fifth project", dueDate: "13.06.2017",created:"12.06.2017",members:"katy",type:"mobile",status:"in progres",customer:"john",icon:""}
		];
		this.modelKopy = this.model;
		this.$dropHeader;
		this.$droppBlock;
		this.$droppBody;
		this.$jsDroppAction;
		this.bool = false;
		this.currentSortedColumn = {};
		
		this.$tbody = document.querySelector('tbody');
		this.$thead = document.querySelector('thead');
		this.$buttonRightMenuShow = document.querySelector('#show-menu');
		this.$buttonRightMenuHide = document.querySelector('.hide-right-menu');
		this.$buttonLeftMenuHide = document.querySelector('.left-menu-hide');
		this.$buttonLeftMenuShow = document.querySelector('.left-menu-show');
		this.$rightBlock = document.querySelector('.right-block');
		this.$leftBlock = document.querySelector('.left-block');
		this.$searchBut = document.querySelector('form input');
		this.$inputsRightBlock = this.$rightBlock.getElementsByTagName('input');
		this.$butCreate = document.querySelector('.but-create');
		this.$valueLabelsRemove = this.$rightBlock.getElementsByTagName('span');
		this.$butOpenList = document.querySelector('.js-dropp-action img');
		this.$butOpenList2 = document.querySelector('.js-dropp-action2 img');
		this.$optionsblock = document.querySelector('.dropp-body');

		this.$tbody.addEventListener('click',this.removeRow.bind(this));
		this.$buttonRightMenuShow.addEventListener('click',this.showRightMenu.bind(this));
		this.$searchBut.addEventListener('keyup',this.keyPressAction.bind(this));
		this.$buttonRightMenuHide.addEventListener('click',this.hideRightMenu.bind(this));
		this.$buttonLeftMenuHide.addEventListener('click',this.hideLeftMenu.bind(this));
		this.$buttonLeftMenuShow.addEventListener('click',this.showLeftMenu.bind(this));
		this.$butOpenList.addEventListener('click',this.droppAction.bind(this));
		this.$butOpenList2.addEventListener('click',this.droppAction.bind(this));
		this.$butCreate.addEventListener('click',this.createNewTablString.bind(this));
		this.$thead.addEventListener('click',this.sortTable.bind(this));
	}
	main() {
		this.drawTable(this.model);
		this.$buttonRightMenuHide.classList.remove('hide-right-menu');
	}
	drawRow(row, index) {
		let $buttonDelete = document.createElement('button');
		$buttonDelete.classList.add('button-delete'); 
		$buttonDelete.setAttribute('rowIndex',index);
		$buttonDelete.classList.add('button-delete');
		let $tr = document.createElement('tr');
		for(let cell in row) {
			const $td = document.createElement('td');
			const $text = document.createTextNode(row[cell]);
				$td.appendChild($text);
				if(cell == 'icon') {
					$td.appendChild($buttonDelete);
				}
				$tr.appendChild($td);
			}
		return $tr;
	}
	removeRow() {
		if(event.target.tagName !== 'BUTTON') {
			return;
		}
		if(!confirm('Do you want to remove this file?')) {
			return;
		}
		const id = event.target.getAttribute('rowIndex');
		this.model.splice(id,1);
		this.redrawTable();
	}
	drawTable(model) {
		for (let i = 0; i < model.length; i++) {
			const $row = this.drawRow(model[i], i);
			this.$tbody.appendChild($row);
		}
	}
	redrawTable() {
		this.$tbody.innerHTML ="";
		this.drawTable(this.model);
	}
	showRightMenu() {
		this.$rightBlock.classList.add('active');
		this.$buttonRightMenuHide.classList.add('hide-right-menu');

	}
	hideRightMenu() {
		this.$rightBlock.classList.remove('active');
		this.$buttonRightMenuHide.classList.remove('hide-right-menu');
	}
	hideLeftMenu() {
		this.$leftBlock.classList.add('active');
		this.$buttonLeftMenuShow.style.visibility = "visible";
	}
	showLeftMenu() {
		this.$leftBlock.classList.remove('active');
		this.$buttonLeftMenuShow.style.visibility = "hidden";
	}
	keyPressAction(event) {
		let elemValue = event.target.value,
			clonedModel = [...this.model],
			str,filtered;
		if(elemValue === "") {
			this.$tbody.innerHTML = "";
			this.drawTable(this.model);
		} else {
			this.$tbody.innerHTML = "";
			filtered = clonedModel.filter((item)=> {
    			return item.projectName.toLowerCase().includes(elemValue.toLowerCase())
  			});
  			for(let i=0;i<filtered.length;i++) {
				str = this.drawRow(filtered[i],i);
				this.$tbody.appendChild(str);
			}
		}
	}
	droppAction(event) {
		let clickfild = event.target;
		this.$jsDroppAction = clickfild.parentNode;
		this.$dropHeader = this.$jsDroppAction.parentNode;
		this.$droppWrapp = this.$dropHeader.parentNode;
		this.$droppBody = this.$droppWrapp.children[1];
		if(this.bool === false) {
			this.$jsDroppAction.style.transform = "rotate(180deg)";
			this.$droppBody.style.overflow = "inherit";
			this.$droppWrapp.style.margin = "0 0 200px 0";
			this.bool = true;
		}else{
			this.$jsDroppAction.style.transform = "";
			this.$droppBody.style.overflow = "hidden";
			this.$droppWrapp.style.margin = "0 0 30px 0";
			this.bool = false;
		}
		this.$droppBody.addEventListener('click',this.selectClick.bind(this));
	}
	selectClick(event) {
        if(this.bool) {
        	let clickfild = event.target;
        	let text = clickfild.innerText;
        	clickfild.children[0].checked = "true";
        	this.$dropHeader.children[0].innerText = text;
		}
		this.$jsDroppAction.style.transform = "";
		this.$droppBody.style.overflow = "hidden";
		this.$droppWrapp.style.margin = "0 0 30px 0";
		this.bool = false;
	}
	createNewTablString() {
		let obj = {}
			obj.projectName = document.querySelector('#input-project-name').value;
			obj.dueDate = document.querySelector('#input-due-data').value;
			obj.created = document.querySelector('#input-created').value;
			obj.members = document.querySelector('#input-members').value;
			obj.type = document.querySelector('.dropp-header-title').innerText;
			obj.status = "in progres";
			obj.customer = document.querySelector('.dropp-header-title2').innerText;
			obj.icon = "";
			this.model.push(obj);
			const $row = this.drawRow(this.model[this.model.length - 1],this.model.length - 1);
			this.$tbody.appendChild($row);
			this.$rightBlock.classList.remove('active');
			this.$buttonRightMenuHide.classList.remove('hide-right-menu');
			this.$valueLabelsRemove[0].innerText ="TYPE:";
			this.$valueLabelsRemove[1].innerText ="CUSTOMER:";
	}
  sortTable() {
	    const columnName = event.target.getAttribute("name"); 
	    if(columnName in this.currentSortedColumn) { 
	      this.currentSortedColumn[columnName] = !this.currentSortedColumn[columnName];
	    } else {
	      this.currentSortedColumn = {}; 
	      this.currentSortedColumn[columnName] = true;
	    }
	    this.model = this.model.sort((a,b) => { 
	      if (this.currentSortedColumn[columnName]) {
	        return (a[columnName] > b[columnName]) ? 1 : -1;
	      } else {
	        return (a[columnName] < b[columnName]) ? 1 : -1;
	      }
	    });
	    this.redrawTable();
 	}
}
document.addEventListener('DOMContentLoaded',()=> {
	this.main = new Main();
	this.main.main();
});
